﻿using Business.Interface;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Customer
{
    public class CustomerBusiness : ICustomerBusiness
    {
        private readonly IUnitOfWork _uow;
        public CustomerBusiness(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void Add(DAL.Models.Customer objInfo)
        {
            _uow.Customers.Add(objInfo);
            _uow.SaveChanges();
        }

        public void Delete(DAL.Models.Customer objInfo)
        {
            _uow.Customers.Remove(objInfo);
            _uow.SaveChanges();
        }

        public void Delete(int id)
        {
            var objInfo = GetById(id);
            Delete(objInfo);
        }

        public IEnumerable<DAL.Models.Customer> GetAll()
        {
            return _uow.Customers.GetAll();
        }

        public DAL.Models.Customer GetById(int id)
        {
            return _uow.Customers.Get(id);
        }

        public void Update(DAL.Models.Customer objInfo)
        {
            _uow.Customers.Update(objInfo);
            _uow.SaveChanges();
        }
    }
}
